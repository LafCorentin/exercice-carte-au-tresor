"use strict";

class aventure {
    map = {}
    explorers = [] // for explorers with non-empty path
    tired_explorers = [] // for explorers with empty path

    constructor(width, height) {
        this.height = height
        this.width = width
    }

    static fromString(input) {
        const lines = input.split("\n").filter(line => line[0] !== "#")

        const firstLine = lines.shift().split(" - ")
        if (firstLine[0] !== "C")
            throw "no C as first line"

        var av = new aventure(firstLine[1], firstLine[2])

        for (var line of lines) {
            var values = line.split(" - ")
            switch (values[0]) {
                case "M":
                    av.add_mountain(parseInt(values[1]), parseInt(values[2]))
                    break
                case "T":
                    av.add_treasure(parseInt(values[1]), parseInt(values[2]), parseInt(values[3]))
                    break
                case "A":
                    av.add_explorer(parseInt(values[2]), parseInt(values[3]), values[1], values[4], values[5])
                    break
            }
        }

        return av
    }

    #isOutofBound(x, y) {
        return x < 0 || y < 0 || x >= this.width || y >= this.height
    }

    #insert(type, x, y, data = undefined) {
        if (this.#isOutofBound(x, y))
            throw "Coordonnées hors limites !"
        if (!this.map[x])
            this.map[x] = {}
        if (!this.map[x][y])
            this.map[x][y] = {
                M : false,
                T : 0,
                A : null
            }

        switch (type) {
            case "M":
                this.map[x][y].M = true
                break
            case "T":
                this.map[x][y].T += data
                break
            case "A":
                this.map[x][y].A = data
                break
        }
    }

    #tryClear(x, y) {
        var pos = this.map[x][y]
        if (pos && pos.T === 0 && pos.M === false && pos.A === null) {
            delete this.map[x][y]
            for (var x in this.map[x])
                return // check if map[x] is empty
            delete this.map[x]
        }
    }

    #queueExplorer(explorer) {
        ((explorer.path == "") ? this.tired_explorers : this.explorers).push(explorer)
    }

    add_mountain(x, y) {
        this.#insert("M", x, y)
    }

    add_treasure(x, y, value) {
        if (typeof (value) !== "number")
            throw "not a number"
        this.#insert("T", x, y, value)
    }

    add_explorer(x, y, name, face, path) {
        this.#insert("A", x, y, name)

        var explorer = {
            name: name,
            x: x,
            y: y,
            treasures: 0,
            face: face,
            path: path
        }

        this.#queueExplorer(explorer)
    }

    getArea(x, y) {
        return this.map[x]?.[y]
    }

    /**
     * @returns the string representing the adventure. Ready to be parsed
     */
    toString() {
        var res = "C - " + this.width + " - " + this.height + "\n"
        var resTres = ""
        for (const [x, column] of Object.entries(this.map)) {
            for (const [y, elem] of Object.entries(column)) {
                if (elem.M)
                    res += "M - " + x + " - " + y + "\n"
                if (elem.T > 0)
                    resTres += "T - " + x + " - " + y + " - " + elem.T + "\n"
            }
        }
        res += resTres

        for (var explorer of this.tired_explorers)
            res += ("A - " + explorer.name + " - " + explorer.x + " - " + explorer.y + " - "
                + explorer.face + " - " + explorer.treasures + "\n")

        return res
    }

    #nextPos(explorer) {
        var next = {}
        switch (explorer.face) {
            case 'S':
                next.x = explorer.x
                next.y = explorer.y + 1
                break
            case 'O':
                next.x = explorer.x - 1
                next.y = explorer.y
                break
            case 'N':
                next.x = explorer.x
                next.y = explorer.y - 1
                break
            case 'E':
                next.x = explorer.x + 1
                next.y = explorer.y
                break
        }
        return next
    }

    // un explorateur qui fait face à une autre lettre que NSEO est perdu : 
    //      il ne pivote plus, ne bouge plus
    #avancer(explorer) {
        var next = this.#nextPos(explorer)

        if (this.#isOutofBound(next.x, next.y))
            return explorer

        //vérifier que la case est accessible
        const occup = this.map[next.x]?.[next.y]
        if (occup?.M || (occup?.A ?? false))
            return explorer

        //avancer
        this.map[explorer.x][explorer.y].A = null
        this.#tryClear(explorer.x, explorer.y)

        if (this.map[next.x]?.[next.y] && this.map[next.x][next.y].T > 0) {
            this.map[next.x][next.y].T-- 
            explorer.treasures++
        }

        explorer.x = next.x
        explorer.y = next.y
        this.#insert("A", explorer.x, explorer.y, explorer.name)

        if (typeof (occup) === 'number')
            explorer.treasures += occup

        return explorer
    }

    #pivotD(explorer) {
        switch (explorer.face) {
            case 'S':
                explorer.face = 'O'
                break
            case 'O':
                explorer.face = 'N'
                break
            case 'N':
                explorer.face = 'E'
                break
            case 'E':
                explorer.face = 'S'
                break
        }
        return explorer
    }

    #pivotG(explorer) {
        switch (explorer.face) {
            case 'S':
                explorer.face = 'E'
                break
            case 'O':
                explorer.face = 'S'
                break
            case 'N':
                explorer.face = 'O'
                break
            case 'E':
                explorer.face = 'N'
                break
        }
        return explorer
    }

    run() {
        //console.debug(this.printFancy())
        var explorer, action
        while (this.explorers.length !== 0) {
            explorer = this.explorers.shift()

            action = explorer.path[0] // on récupère la première lettre du path
            explorer.path = explorer.path.substring(1) // et on l'enlève du path

            switch (action) {
                case 'A':
                    explorer = this.#avancer(explorer)
                    break
                case 'D':
                    explorer = this.#pivotD(explorer)
                    break
                case 'G':
                    explorer = this.#pivotG(explorer)
                    break
            }

            this.#queueExplorer(explorer)
            //console.log(this.printFancy())
        }
    }

    printFancy() {
        var res = ""
        for (let y = 0; y < this.height; y++) {
            for (let x = 0; x < this.width; x++) {
                var area = this.getArea(x, y)
                if (area === undefined)
                    res+= ". "
                else if (area.M)
                    res += "M "
                else if (area.A)
                    res += "A "
                else if (area.T > 0)
                    res += "T "
                else
                    res += ". "
            }
            res += "\n"
        }

        return res
    }
}

exports.aventure = aventure