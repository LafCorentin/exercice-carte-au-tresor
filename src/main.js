const fs = require("fs")
const { aventure } = require("../src/aventure")

async function main() {
    const inputFile = "input.txt"
    const outputFile = "output.txt"

    console.log("Lecture de :")

    file = await fs.readFileSync(inputFile,
        { encoding: 'utf8', flag: 'r' })

    console.log(typeof(file), file)
    
    const av = aventure.fromString(file)
    
    console.log("Simulation en cours...")
    av.run()
    console.log("Résultats :")

    const res = av.toString()
    
    await fs.writeFileSync(outputFile, res)
    
    console.log(res)
    console.log("terminé")
}

main()