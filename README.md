# Carte au trésor

Petit projet pour répondre à l'écercice de la Carte au trésor.

La complexité est bonne. La structure du code doit être revue pour ajouter plus d'options, dépendament desquelles.

## Installation

Exemple avec npm (mais pnpm c'est mieux) :
```shell
npm install
```

## Utilisation

### Test

```shell
npm run test
```

### Simulation

Mettez en premier lieu votre input dans input.txt

```shell
npm start 
```

Récupérez votre résultat dans output.txt