const { aventure } = require("../src/aventure")

test('Tests ajouts objets', () => {

    av1 = new aventure(5, 7)

    expect(av1.width).toBe(5)
    expect(av1.height).toBe(7)

    av1.add_mountain(1, 1)

    //check hors limite
    expect(() => av1.add_mountain(-1, 0)).toThrow()
    expect(() => av1.add_mountain(0, -1)).toThrow()
    expect(() => av1.add_mountain(8, 0)).toThrow()
    expect(() => av1.add_mountain(0, 8)).toThrow()

    //check sommes tresors
    av1.add_treasure(2, 2, 5)
    av1.add_treasure(2, 2, 5)
    expect(av1.getArea(2, 2).T).toBe(10)
})

test("Tests de collisions", () => {

    // A M .    A M .
    // B T C -> . B C
    // . . .    . . . 
    av = new aventure(3, 3)
    av.add_mountain(1, 0)
    av.add_treasure(1, 1, 999)
    av.add_explorer(0, 0, "Alice", "E", "A")
    av.add_explorer(0, 1, "Bob", "E", "AA")
    av.add_explorer(2, 1, "Coco", "E", "A")

    av.run()

    expect(av.getArea(0, 0).A).toBe("Alice")
    expect(av.getArea(1, 1).A).toBe("Bob")
    expect(av.getArea(2, 1).A).toBe("Coco")
    expect(av.getArea(1, 2)).toBeUndefined()

    res = av.toString()

    for (line of res.split("\n"))
        if (line.includes("Bob"))
            expect(line).toMatch(/.*E - 1.*/)
})

test("Tests de guidage", () => {
    // A1 gène A2 puis celui ci fait le tour
    // A2 M M    A2 M M    . M  M
    // .  . . -> A1 . . -> . A2 A1
    // A1 . .    .  . .    . .  .
    av = new aventure(3, 3)
    av.add_mountain(1, 0)
    av.add_mountain(2, 0)
    av.add_explorer(0, 2, "Alice", "N", "ADA")
    av.add_explorer(0, 0, "Bob", "S", "AAAAGAAGA") //deux A sont bloqués par Alice, 
                                                    //qui arrive juste avant puis pivote

    av.run()

    expect(av.getArea(1, 1).A).toBe("Alice")
    expect(av.getArea(2, 1).A).toBe("Bob")
})

test("test read/print string", () => {
    input = `C - 2 - 4
M - 1 - 0
A - Alice - 0 - 0 - S - AA
T - 0 - 1 - 2
T - 0 - 2 - 1
T - 1 - 1 - 7
`
    av = aventure.fromString(input)

    av.run()

    res = av.toString()

    expect(res).toMatch(/C - 2 - 4/)
    expect(res).toMatch(/M - 1 - 0/)
    expect(res).toMatch(/T - 1 - 1 - 7/)
    expect(res).toMatch(/T - 0 - 1 - 1/)
    expect(res).toMatch(/A - Alice - 0 - 2 - S - 2/)
    
})